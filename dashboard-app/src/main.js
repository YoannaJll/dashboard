// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VModal from 'vue-js-modal'


import Vuex from 'vuex'
import VueCharts from 'vue-chartjs'
import store from './vuex/store'

Vue.config.productionTip = false
Vue.use(BootstrapVue, VModal, Vuex, VueCharts)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: {App},
  template: '<App/>',
  router,
  store
})
