import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import ProductsList from '@/components/ProductsList'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/articles',
      name: 'ProductsList',
      component: ProductsList
    }
  ]
})
