import Vue from 'vue'
import Vuex from 'vuex'
import ProductsModule from './products'
import ColorsModule from './colors'

Vue.use(Vuex)
const store = new Vuex.Store({
  modules: {
    products: ProductsModule,
    colors: ColorsModule
  }
})

export default store
