
  const defaultState = {
    checkedColor: '#16B0B3',
    categoryColor: ['#139799','#16B0B3','#5a6268','#3e8888'],
    activeTheme: 'blue',
    themes: {
      blue: ['#139799','#16B0B3','#5a6268','#3e8888'],
      red: ['#FF5458','#B23A52','#5a6268','#7F3645']
    }
  }
  const mutations = {
    CHANGE_COLOR (state, theme) {
      state.activeTheme = theme
      if(state.activeTheme === 'blue'){
        state.checkedColor = '#16B0B3'
        state.categoryColor = state.themes.blue
      }
      else{
        state.checkedColor = '#FF5458'
        state.categoryColor = state.themes.red
      }
    }
  }

  export default {
    state: defaultState,
    mutations
  }
