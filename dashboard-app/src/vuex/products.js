import axios from 'axios'

  const defaultState = {
    products: []
  }
  const actions = {
    GET_ALL_PRODUCTS: async function ({commit}) {
      const req = await axios.get('http://localhost:3000/notes')
      return commit('SET_LIST', {list: req.data })
    },
    ADD_TO_LIST: async function ({commit}, payload) {
      const note = await axios.post('http://localhost:3000/notes', {...payload, price: `$${payload.price}`})
      return commit('ADD_TO_LIST', { note : note.data })
    },
    DELETE_PRODUCT: async function({commit}, payload){
      await axios.delete(`http://localhost:3000/notes/${payload}`)
      return commit('DELETE_PRODUCT', { id : payload })
    },
    UPDATE_PRODUCT: async function({commit}, payload){
      const itemSelected = axios.put(`http://localhost:3000/notes/${payload._id}`, payload)
      return commit('UPDATE_PRODUCT', { itemSelected: itemSelected.data })
    }
  }
  const mutations = {
    SET_LIST: (state, { list }) => {
      state.products = list
    },
    ADD_TO_LIST: (state, { note }) => {
      state.products = [ ...state.products, note ]
    },
    DELETE_PRODUCT: (state, { id }) => {
      state.products.splice(state.products.findIndex(item => item.id === id),1)
    },
    UPDATE_PRODUCT: (state, {itemSelected}) => {
      state.products = [ ...state.products, itemSelected]
    }
  }
  const getters = {
    salesProduct: state => {
      return state.products.filter(product => product.sales === true).length
    },
    getAllCategories: state => {
      return [...new Set(state.products.map(cat => cat.productCategory))]
    },
    getAllCountries: state => {
      return [...new Set(state.products.map(cat => cat.country))]
    },
    totalSalesPriceProduct: state => state.products.reduce((a, b) => a + parseInt(b.price.replace('$','')), 0)
    ,
    getAllProductByDate: state => {
      return state.products.map(product => product.date)
    }
  }

export default {
  state: defaultState,
  getters,
  actions,
  mutations
}
