const mongoose = require('mongoose');

const NoteSchema = mongoose.Schema({
    id: String,
    productCategory: String,
    price: String,
    date: Date,
    country: String,
    sales: Boolean,
    productName: String,
    description: String
}, {
    timestamps: true
});

module.exports = mongoose.model('Note', NoteSchema);