module.exports = (app) => {
    const products = require('../controllers/note.controller.js');

    // Create a new Note
    app.post('/notes', products.create);

    // Retrieve all Notes
    app.get('/notes', products.findAll);

    // Retrieve a single Note with noteId
    app.get('/notes/:noteId', products.findOne);

    // Update a Note with noteId
    app.put('/notes/:noteId', products.update);

    // Delete a Note with noteId
    app.delete('/notes/:noteId', products.delete);
}